<?php
$sql = "SELECT * FROM `job_seeker` WHERE `post_id` = :id;";
$result = $db->prepare($sql);
$result->bindParam(':id', $_GET['id'], PDO::PARAM_INT);
$result->execute();
$row = $result->fetch();
?>
<form action="index.php?m=update&id=<?=$_GET['id'];?>" method="post">
    <table class="table table-bordered w-50 mx-auto">
        <input type="hidden" name="method" value="update" style="display: none;">
        <tr>
            <td>公告編號:</td>
            <td><input type="text" name="post_id" value="<?=$row->post_id;?>" readonly class="form-control"></td>
        </tr>
        <tr>
            <td>求才廠商:</td>
            <td><input type="text" name="supplier" value="<?=$row->supplier;?>" class="form-control"></td>
        </tr>
        <tr>
            <td>求才內容:</td>
            <td><textarea name="content" rows="3" placeholder="求才內容" class="form-control"><?=$row->content;?></textarea></td>
        </tr>
        <tr>
            <td>求才日期:</td>
            <td><input type="text" name="date" value="<?=$row->date;?>" readonly class="form-control"></td>
        </tr>
        <tr>
            <td colspan="2"><button type="submit" class="btn btn-primary">修改</button></td>
        </tr>
    </table>
</form>
<?php
if(isset($_POST['method']) && $_POST['method'] == "update"){
    $d = date('Y-m-d H:i:s', strtotime($_POST['date']));
    $sql = "UPDATE `job_seeker` SET `supplier` = :supplier, `content` = :content, 
      `date` = CURRENT_DATE() WHERE `post_id` = :post_id;";
    $result = $db->prepare($sql);
    $result->bindParam(':post_id', $_POST['post_id'], PDO::PARAM_INT);
    $result->bindParam(':supplier', $_POST['supplier'], PDO::PARAM_STR);
    $result->bindParam(':content', $_POST['content'], PDO::PARAM_STR);
    if($result->execute()){
        echo "修改成功！";
    }else{
        echo "修改失敗！";
    }
}

