<form action="index.php?m=insert" method="post">
    <table class="table table-bordered w-50 mx-auto">
        <input type="hidden" name="method" value="insert" style="display: none;">
        <tr>
            <td>求才廠商:</td>
            <td><input type="text" name="supplier" class="form-control"></td>
        </tr>
        <tr>
            <td>求才內容:</td>
            <td><textarea name="content" rows="3" placeholder="求才內容" class="form-control"></textarea></td>
        </tr>
        <tr>
            <td>求才日期:</td>
            <td><input type="text" name="date" value="<?=date("Y-m-d");?>" readonly class="form-control"></td>
        </tr>
        <tr>
            <td colspan="2"><button type="submit" class="btn btn-primary">新增</button></td>
        </tr>
    </table>
</form>
<?php
    if(isset($_POST['method']) && $_POST['method'] == "insert"){
        $sql = "INSERT INTO `job_seeker` (`supplier`, `content`, `date`) 
        VALUES (:supplier, :content, CURRENT_DATE());";
        $result = $db->prepare($sql);
        $result->bindParam(':supplier', $_POST['supplier'], PDO::PARAM_STR);
        $result->bindParam(':content', $_POST['content'], PDO::PARAM_STR);
        if($result->execute()){
            echo "新增成功！";
        }else{
            echo "新增失敗！";
        }
    }
