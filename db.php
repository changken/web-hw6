<?php
require_once "db_config.php";

$dsn = "mysql:dbhost=".DB_HOSTNAME.";dbname=".DB_NAME.";charset=utf8";
try{
    $db = new PDO($dsn, DB_USER, DB_PW);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
}catch (PDOException $e){
    echo $e->getMessage()."<br/>";
}

