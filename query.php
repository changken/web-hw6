<form action="index.php?m=query" method="post" class="form-inline mb-3 justify-content-center">
    <input type="text" name="searchtext" value="<?php echo (isset($_POST['searchtext'])) ? $_POST['searchtext'] : "";?>" placeholder="要查詢的值" class="form-control">
    <select name="type" class="custom-select">
        <option value="id" selected>公告編號</option>
        <option value="supplier">求才廠商</option>
        <option value="content">求才內容</option>
    </select>
    <button type="submit" class="btn btn-primary ml-3">查詢</button>
</form>
<?php
if(isset($_POST['searchtext'])){
    if($_POST['type'] == "id")
        $sql = "SELECT * FROM `job_seeker` WHERE `post_id` = :id;";
    elseif($_POST['type'] == "supplier"){
        $sql = "SELECT * FROM `job_seeker` WHERE `supplier` LIKE '%' :supplier '%';";
    }else{
        $sql = "SELECT * FROM `job_seeker` WHERE `content` LIKE '%' :content '%';";
    }
}else{
    $sql = "SELECT * FROM `job_seeker`;";
}
$result = $db->prepare($sql);
if(isset($_POST['searchtext'])){
    if($_POST['type'] == "id"){
        $result->bindParam(':id', $_POST['searchtext'], PDO::PARAM_INT);
    }elseif($_POST['type'] == "supplier"){
        $result->bindParam(':supplier', $_POST['searchtext'], PDO::PARAM_STR);
    }else{
        $result->bindParam(':content', $_POST['searchtext'], PDO::PARAM_STR);
    }
}
$result->execute();
//$result->debugDumpParams();
$data = $result->fetchAll();
?>
<table class="table table-bordered w-75 mx-auto">
    <thead class="thead-dark">
        <tr>
            <th>公告編號</th>
            <th>求才廠商</th>
            <th>求才內容</th>
            <th>求才日期</th>
            <th>動作</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data as $row): ?>
            <tr>
                <td><?=$row->post_id;?></td>
                <td><?=$row->supplier;?></td>
                <td><?=$row->content;?></td>
                <td><?=$row->date;?></td>
                <td>
                    <a href="index.php?m=update&id=<?=$row->post_id;?>" class="btn btn-warning">修改</a>&nbsp;
                    <a href="index.php?m=delete&id=<?=$row->post_id;?>" class="btn btn-danger" onclick="return check();">刪除</a>
                </td>
            </tr>
        <?php endforeach;?>
    </tbody>
</table>